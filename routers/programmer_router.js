const express = require('express')

const programmer_controller = require('../controllers/programmer_controller')

const route = express.Router()

route.post('/signup',programmer_controller.signup)
route.post('/signin',programmer_controller.signin)

module.exports = route