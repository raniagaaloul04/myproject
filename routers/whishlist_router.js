const express = require('express')

const whishlist_controller = require('../controllers/whishlist_controller')

const route = express.Router()

route.post('/add',whishlist_controller.add)
route.get('/all',whishlist_controller.GetAllWhishsLists)



module.exports = route