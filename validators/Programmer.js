const joi = require('@hapi/joi')

const validatesignData = data => {
    const schema = joi.object({


        Name: joi
            .string()
            .min(3)
            .max(15)
            .required(),
        Password: joi
            .string()
            .min(3)
            .max(15)
            .alphanum()
            .required(),
    })
    return schema.validate(data, { abortEarly: false })
 
}
module.exports = {
    validatesignData ,


}