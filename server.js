const express = require('express');
const ProgrammerRouter=require("./routers/programmer_router")
const ProductRouter=require("./routers/product_router")
const WhishlistRouter=require("./routers/whishlist_router")
const db=require("./config/DataBase")
const app = express();
app.use(require('cors')())
app.use(express.json());
app.use(express.urlencoded());
app.use("/programmers",ProgrammerRouter)
app.use("/products",ProductRouter)
app.use("/whishlists",WhishlistRouter)
app.get('/getfile/:image',function(req,res){//pour faire affichage image sur postman
    res.sendFile(__dirname+'/images/'+req.params.image)
})
app.listen(5000, () => {
    console.log("start")
})

